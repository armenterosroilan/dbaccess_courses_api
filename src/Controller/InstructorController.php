<?php


namespace App\Controller;

use App\Repository\InstructorRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InstructorSiteController
 * @package App\Controller
 *
 * @Route(path="/instructor")
 */
class InstructorController
{
    private $instructorRepository;

    public function __construct(InstructorRepository $instructorRepository)
    {
        $this->instructorRepository = $instructorRepository;
    }

    /**
     * @Route("/get-all", name="get_all_instructors", methods={"GET"})
     */
    public function getAllInstructors(): JsonResponse
    {
        $instructors = $this->instructorRepository->findAll();
        $data = [];

        foreach ($instructors as $instructor) {
            $data[] = [
                'id' => $instructor->getId(),
                'Name' => $instructor->getName(),
                'Age' => $instructor->getAge(),
                'start' => $instructor->getStartPeriod(),
                'end' => $instructor->getEndPeriod(),
                'evaluation' => $instructor->getEvaluation(),
                'course' => $instructor->getCourse()->getId()
            ];
        }

        return new JsonResponse(['instructor' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/add", name="add_instructor", methods={"POST"})
     */
    public function addInstructor(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $Name = $data['Name'];
        $age = $data['age'];
        $startPeriod = $data['startPeriod'];
        $endPeriod = $data['endPeriod'];
        $evaluation = $data['evaluation'];

        if (empty($Name) || empty($age) || empty($startPeriod) || empty($endPeriod) || empty($evaluation)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->instructorRepository->saveInstructor($Name, $age, $startPeriod, $endPeriod, $evaluation);

        return new JsonResponse(['status' => 'Instructor added!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/get/{id}", name="get_one_instructor", methods={"GET"})
     */
    public function getOneInstructor($id): JsonResponse
    {
        $instructor = $this->instructorRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $instructor->getId(),
            'Name' => $instructor->getName(),
            'age' => $instructor->getAge(),
            'startPeriod' => $instructor->getStartPeriod(),
            'endPeriod' => $instructor->getEndPeriod(),
            'evaluation' => $instructor->getEvaluation(),
        ];

        return new JsonResponse(['instructor' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/update/{id}", name="update_instructor", methods={"PUT"})
     */
    public function updateInstructor($id, Request $request): JsonResponse
    {
        $instructor = $this->instructorRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        $this->instructorRepository->updateInstructor($instructor, $data);

        return new JsonResponse(['status' => 'instructor updated!']);
    }

    /**
     * @Route("/delete/{id}", name="delete_instructor", methods={"DELETE"})
     */
    public function deleteInstructor($id): JsonResponse
    {
        $instructor = $this->instructorRepository->findOneBy(['id' => $id]);

        $this->instructorRepository->removeInstructor($instructor);

        return new JsonResponse(['status' => 'instructor deleted']);
    }


}