<?php


namespace App\Controller;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Guesser\Name;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InstructorSiteController
 * @package App\Controller
 *
 * @Route(path="/course")
 */
class CourseController
{
    private $courseRepository;

    public function __construct(CourseRepository $courseRepository){
        $this->courseRepository = $courseRepository;
    }

    /**
     * @Route("/get-all", name="get_all_courses", methods={"GET"})
     */
    public function getAllCourses(): JsonResponse
    {
        $courses = $this->courseRepository->findAll();

        foreach ($courses as $course) {


            $data[] = [
                'id' => $course->getId(),
                'Name' => $course->getName(),
                'start' => $course->getStartDate(),
                'end' => $course->getEndDate(),
                'instructors' => $course->getInstructors()->toArray()
            ];
        }

        return new JsonResponse(['course' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/add", name="add_course", methods={"POST"})
     */
    public function addCourse(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $Name = $data['Name'];
        $startDate = $data['startDate'];
        $endDate = $data['endDate'];
        $instructors = $data['instructors'];

        if (empty($Name) ||  empty($startDate) || empty($endDate)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $this->courseRepository->saveCourse($Name, $startDate, $endDate, $instructors);

        return new JsonResponse(['status' => 'Course added!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/get/{id}", name="get_one_course", methods={"GET"})
     */
    public function getOneCourse($id): JsonResponse
    {
        $course = $this->courseRepository->findOneBy(['id' => $id]);

        $data = [
            'id' => $course->getId(),
            'Name' => $course->getName(),
            'startDate' => $course->getStartDate(),
            'endDate' => $course->getEndDate(),
            'instructors' => $course->getInstructorsID(),
        ];

        return new JsonResponse(['course' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/update/{id}", name="update_course", methods={"PUT"})
     */
    public function updateCourse($id, Request $request): JsonResponse
    {
        $course = $this->courseRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        $this->courseRepository->updateCourse($course, $data);

        return new JsonResponse(['status' => 'Course updated!']);
    }

    /**
     * @Route("/delete/{id}", name="delete_course", methods={"DELETE"})
     */
    public function deleteCourse($id): JsonResponse
    {
        $course = $this->courseRepository->findOneBy(['id' => $id]);

        $this->courseRepository->removeCourse($course);

        return new JsonResponse(['status' => 'Course deleted']);
    }

}