<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Customer;
use App\Entity\Instructor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $instructor1 = new Instructor();
            $instructor2 = new Instructor();

            $instructor1->setName($faker->firstName);
            $instructor1->setAge($faker->biasedNumberBetween(30,45));
            $instructor1->setEvaluation($faker->biasedNumberBetween(1,5));
            $instructor1->setStartPeriod($faker->dateTime);
            $instructor1->setEndPeriod($faker->dateTime);
            $manager->persist($instructor1);

            $instructor2->setName($faker->firstName);
            $instructor2->setAge($faker->biasedNumberBetween(30,45));
            $instructor2->setEvaluation($faker->biasedNumberBetween(1,5));
            $instructor2->setStartPeriod($faker->dateTime);
            $instructor2->setEndPeriod($faker->dateTime);
            $manager->persist($instructor2);

            $course = new Course();
            $course->setName($faker-> domainName);
            $course->setStartDate($faker->dateTime);
            $course->setEndDate($faker->dateTime);
            $course->addInstructor($instructor1);
            $course->addInstructor($instructor2);
            $manager->persist($course);
        }

        $manager->flush();
    }
}
