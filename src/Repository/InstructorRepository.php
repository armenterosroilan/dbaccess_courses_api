<?php

namespace App\Repository;

use App\Entity\Instructor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Instructor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Instructor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Instructor[]    findAll()
 * @method Instructor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstructorRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Instructor::class);
        $this->manager = $manager;
    }

    public function saveInstructor($Name, $age, $startPeriod, $endPeriod, $evaluation)
    {
        $newInstructor = new Instructor();

        $newInstructor
            ->setName($Name)
            ->setAge($age)
            ->setStartPeriod($startPeriod)
            ->setEndPeriod($endPeriod)
            ->setEvaluation($evaluation);

        $this->manager->persist($newInstructor);
        $this->manager->flush();
    }

    public function updateInstructor(Instructor $instructor, $data)
    {
        empty($data['Name']) ? true : $instructor->setName($data['Name']);
        empty($data['age']) ? true : $instructor->setAge($data['age']);
        empty($data['startPeriod']) ? true : $instructor->setStartPeriod($data['startPeriod']);
        empty($data['endPeriod']) ? true : $instructor->setEndPeriod($data['endPeriod']);
        empty($data['evaluation']) ? true : $instructor->setEvaluation($data['evaluation']);
        empty($data['course']) ? true : $instructor->setCourse($data['course']);
        $this->manager->flush();
    }

    public function removeInstructor(Instructor $instructor)
    {
        $this->manager->remove($instructor);
        $this->manager->flush();
    }

}
