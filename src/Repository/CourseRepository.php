<?php

namespace App\Repository;

use App\Entity\Course;
use App\Entity\Instructor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Course|null find($id, $lockMode = null, $lockVersion = null)
 * @method Course|null findOneBy(array $criteria, array $orderBy = null)
 * @method Course[]    findAll()
 * @method Course[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Course::class);
        $this->manager = $manager;
    }

    public function saveCourse($Name, $startDate, $endDate, $instructors)
    {
        $newCourse = new Course();

        $newCourse
            ->setName($Name)
            ->setStartDate($startDate)
            ->setEndDate($endDate);
        foreach ($instructors as $instructor) {
            $newCourse->addInstructor($instructor);
        }

        $this->manager->persist($newCourse);
        $this->manager->flush();
    }

    public function updateCourse(Course $course, $data)
    {
        empty($data['Name']) ? true : $course->setName($data['Name']);
        empty($data['startDate']) ? true : $course->setStartDate($data['startDate']);
        empty($data['endDate']) ? true : $course->setEndDate($data['endDate']);
        empty($data['instructors']) ? true : $course->updateInstructors($data['instructors']);
        $this->manager->flush();
    }


    public function removeCourse(Course $course)
    {
        $this->manager->remove($course);
        $this->manager->flush();
    }



}
